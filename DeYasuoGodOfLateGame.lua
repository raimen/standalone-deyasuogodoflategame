if not myHero.charName == "Yasuo" then return end
--[[
	-Setup to Prioritize enemies <==Done
	-Summoner spells

]]

--[[VARS]]--
local EnemyTable


--[[VARS]]--

function TickManager:__init(ticksPerSecond)
        self.TPS = ticksPerSecond
        self.lastClock = 0
        self.currentClock = 0
end
 
function TickManager:__type()
        return "TickManager"
end
 
function TickManager:setTPS(ticksPerSecond)
        self.TPS = ticksPerSecond
end
 
function TickManager:getTPS(ticksPerSecond)
        return self.TPS
end
 
function TickManager:isReady()
        self.currentClock = os.clock()
        if self.currentClock < self.lastClock + (1 / self.TPS) then return false end
        self.lastClock = self.currentClock
        return true
end
 
local tm = TickManager(20)
 
function OnTick()
	SummonerOnTick()
end

function onLoad()
	
	ignite = (player:GetSpellData(SUMMONER_1).name == "SummonerDot" and SUMMONER_1 or (player:GetSpellData(SUMMONER_2).name == "SummonerDot" and SUMMONER_2 or nil))
    barrier = (player:GetSpellData(SUMMONER_1).name == "SummonerBarrier" and SUMMONER_1 or (player:GetSpellData(SUMMONER_2).name == "SummonerBarrier" and SUMMONER_2 or nil))

	createMenu()
	EnemyTable = GetEnemyHeroes()
	arrangePrioritys()


	 
	PrintChat("<font color='#CCCCCC'> >> Yasuo - The God of LateGame! <<</font>")
end

-------------------------Summoner spells----------------------------
 function SummonerOnTick()
        if ignite and YasuoConfig.Summ.autoIgnite and myHero:CanUseSpell(ignite) == READY then
                for _, enemy in pairs(EnemyTable) do
                        if ValidTarget(enemy, 600) and enemy.health <= 50 + (20 * player.level) then
                                CastSpell(ignite, enemy)
                        end
                end
        end
        if barrier and YasuoConfig.Summ.autoBarrier and myHero:CanUseSpell(barrier) == READY then
                if GetTickCount() >= nextCheck then
                        local co = ((myHero.health / myHero.maxHealth * 100) - 20)*(0.3-0.1)/(100-20)+0.1
                        local proc = myHero.maxHealth * co
                        if healthBefore - myHero.health > proc and myHero.health < myHero.maxHealth * 0.3 then
                                CastSpell(barrier)
                        end
                        nextCheck = GetTickCount() + 100
                        if GetTickCount() >= nextUpdate then
                                healthBefore = myHero.health
                                healthBeforeTimer = GetTickCount()
                                nextUpdate = GetTickCount() + 1000
                        end
                end
        end
end
-------------------------Summoner spells----------------------------

------------------Priority Setting-------------
function SetPriority(table, hero, priority)
    for i=1, #table, 1 do
        if hero.charName:find(table[i]) ~= nil then
            TS_SetHeroPriority(priority, hero.charName)
			return
        end
    end
end
 
function arrangePrioritys()
    for i, enemy in ipairs(EnemyTable) do
        SetPriority(priorityTable.AD_Carry, enemy, 1)
        SetPriority(priorityTable.AP,       enemy, 2)
        SetPriority(priorityTable.Support,  enemy, 3)
        SetPriority(priorityTable.Bruiser,  enemy, 4)
        SetPriority(priorityTable.Tank,     enemy, 5)
    end
end
------------------Priority Setting-------------

function createMenu()
	 YasuoConfig = scriptConfig("Yasuo - The God of LateGame", "Yasuo_The_Unforgiven")
     --> Basic
     YasuoConfig:addSubMenu("Basic Settings", "Basic")
	 YasuoConfig.Basic:addParam("KillDemAll", "Kill dem ALL", SCRIPT_PARAM_ONKEYDOWN, false, 32)---< Working on it
	 YasuoConfig.Basic:addParam("ignoreTowers", "Ignore - Turrets", SCRIPT_PARAM_ONOFF, false)
	 --> Auto
     YasuoConfig:addSubMenu("Auto Settings", "Auto")        
	 YasuoConfig.Auto:addParam("autoQ", "Steel Tempest - Auto Poke Enemy Heroes", SCRIPT_PARAM_ONOFF, false)
     YasuoConfig.Auto:addParam("qStacker", "Steel Tempest - Auto Stack", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("V"))
	 --> Summoner
	YasuoConfig:addSubMenu("Summoner Settings", "Summ")
	if ignite ~= nil then
		YasuoConfig.Summ:addParam("autoIgnite", "Use Ignite if killable", SCRIPT_PARAM_ONOFF, false)
	end
	if barrier ~= nil then
		YasuoConfig.Summ:addParam("autoBarrier", "Use Shield on low hp", SCRIPT_PARAM_ONOFF, false)
	end
	--> Farm
    YasuoConfig:addSubMenu("Farm Settings", "Farm")
    YasuoConfig.Farm:addParam("eFarm", "Farm - Sweeping Blade (G)", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("G"))
	YasuoConfig.Farm:addParam("eFarmCheckHealth", "Farm - Sweeping Blade Only lasthit", SCRIPT_PARAM_ONOFF,true)
	YasuoConfig.Farm:addParam("eFarmUseQ", "Farm - Sweeping Blade Uses Q", SCRIPT_PARAM_ONOFF,false)
	YasuoConfig.Farm:permaShow("eFarm")
	YasuoConfig.Auto:permaShow("qStacker")
	YasuoConfig.Basic:permaShow("KillDemAll")
end

-->Our data arrays

--From AutoCarry Ty

--[[ Items ]]--
local items =
	{
		{name = "Blade of the Ruined King", menu = "BRK", id=3153, range = 450, reqTarget = true, slot = nil },
		{name = "Bilgewater Cutlass", menu = "BWC", id=3144, range = 450, reqTarget = true, slot = nil },
		{name = "Deathfire Grasp", menu = "DFG", id=3128, range = 750, reqTarget = true, slot = nil },
		{name = "Hextech Gunblade", menu = "HGB", id=3146, range = 400, reqTarget = true, slot = nil },
		{name = "Ravenous Hydra", menu = "RSH", id=3074, range = 350, reqTarget = false, slot = nil},
		{name = "Sword of the Divine", menu = "STD", id=3131, range = 350, reqTarget = false, slot = nil},
		{name = "Tiamat", menu = "TMT", id=3077, range = 350, reqTarget = false, slot = nil},
		{name = "Entropy", menu = "ETR", id=3184, range = 350, reqTarget = false, slot = nil},
		{name = "Youmuu's Ghostblade", menu = "YGB", id=3142, range = 350, reqTarget = false, slot = nil}
	}


priorityTable = {
 
    AP = {
        "Annie", "Ahri", "Akali", "Anivia", "Annie", "Brand", "Cassiopeia", "Diana", "Evelynn", "FiddleSticks", "Fizz", "Gragas", "Heimerdinger", "Karthus",
        "Kassadin", "Katarina", "Kayle", "Kennen", "Leblanc", "Lissandra", "Lux", "Malzahar", "Mordekaiser", "Morgana", "Nidalee", "Orianna",
        "Rumble", "Ryze", "Sion", "Swain", "Syndra", "Teemo", "TwistedFate", "Veigar", "Viktor", "Vladimir", "Xerath", "Ziggs", "Zyra", "MasterYi",
    },
    Support = {
        "Alistar", "Blitzcrank", "Janna", "Karma", "Leona", "Lulu", "Nami", "Nunu", "Sona", "Soraka", "Taric", "Thresh", "Zilean",
    },
 
    Tank = {
        "Amumu", "Chogath", "DrMundo", "Galio", "Hecarim", "Malphite", "Maokai", "Nasus", "Rammus", "Sejuani", "Shen", "Singed", "Skarner", "Volibear",
        "Warwick", "Yorick", "Zac",
    },
 
    AD_Carry = {
        "Ashe", "Caitlyn", "Corki", "Draven", "Ezreal", "Graves", "Jayce", "KogMaw", "MissFortune", "Pantheon", "Quinn", "Shaco", "Sivir","Jinx",
        "Talon", "Tristana", "Twitch", "Urgot", "Varus", "Vayne", "Zed","Lucian",
 
    },
 
    Bruiser = {
        "Aatrox", "Darius", "Elise", "Fiora", "Gangplank", "Garen", "Irelia", "JarvanIV", "Jax", "Khazix", "LeeSin", "Nautilus", "Nocturne", "Olaf", "Poppy",
        "Renekton", "Rengar", "Riven", "Shyvana", "Trundle", "Tryndamere", "Udyr", "Vi", "MonkeyKing", "XinZhao","Yasuo",
    },
 
}

		